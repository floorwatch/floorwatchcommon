package com.floorwatch.common.pojos;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SimpleNetwork {

    public enum Network {
        Facebook, Twitter
    }
    
    private String description;
    private String uid;
    private String profilePicture;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }    

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }
    
}
