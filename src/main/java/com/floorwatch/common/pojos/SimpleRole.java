package com.floorwatch.common.pojos;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SimpleRole {

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }    
}
