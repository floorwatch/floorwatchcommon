package com.floorwatch.common.pojos;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SimpleUser {
    
    private int id;
    private String userName;
    private String password;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String firebaseToken;
    private List<SimpleNetwork> networks;
    private List<SimpleRole> roles;
    private List<SimpleStore> stores;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    
    public List<SimpleNetwork> getNetworks() {
        return networks;
    }

    public void setNetworks(List<SimpleNetwork> networks) {
        this.networks = networks;
    }

    public List<SimpleRole> getRoles() {
        return roles;
    }

    public void setRoles(List<SimpleRole> roles) {
        this.roles = roles;
    }

    public List<SimpleStore> getStores() {
        return stores;
    }

    public void setStores(List<SimpleStore> stores) {
        this.stores = stores;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }
}
