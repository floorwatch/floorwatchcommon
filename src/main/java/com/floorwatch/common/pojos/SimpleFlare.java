package com.floorwatch.common.pojos;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SimpleFlare {

    private int id;
    private SimpleStore store;
    private SimpleUser customerUser;
    private SimpleUser managerUser;
    private String customerText;
    private String managerText;
    private String customerFollowupText;
    private String managerFollowupText;
    private boolean resolved;
    private Long createdAt;
    private Long resolvedAt;
    private Integer resolvedStars;
    private String resolvedText;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SimpleStore getStore() {
        return store;
    }

    public void setStore(SimpleStore store) {
        this.store = store;
    }

    public SimpleUser getCustomerUser() {
        return customerUser;
    }

    public void setCustomerUser(SimpleUser customerUser) {
        this.customerUser = customerUser;
    }

    public SimpleUser getManagerUser() {
        return managerUser;
    }

    public void setManagerUser(SimpleUser managerUser) {
        this.managerUser = managerUser;
    }

    public String getCustomerText() {
        return customerText;
    }

    public void setCustomerText(String customerText) {
        this.customerText = customerText;
    }

    public String getManagerText() {
        return managerText;
    }

    public void setManagerText(String managerText) {
        this.managerText = managerText;
    }

    public String getCustomerFollowupText() {
        return customerFollowupText;
    }

    public void setCustomerFollowupText(String customerFollowupText) {
        this.customerFollowupText = customerFollowupText;
    }

    public String getManagerFollowupText() {
        return managerFollowupText;
    }

    public void setManagerFollowupText(String managerFollowupText) {
        this.managerFollowupText = managerFollowupText;
    }

    public boolean isResolved() {
        return resolved;
    }

    public void setResolved(boolean resolved) {
        this.resolved = resolved;
    }

    public Long getResolvedAt() {
        return resolvedAt;
    }

    public void setResolvedAt(Long resolvedAt) {
        this.resolvedAt = resolvedAt;
    }

    public Integer getResolvedStars() {
        return resolvedStars;
    }

    public void setResolvedStars(Integer resolvedStars) {
        this.resolvedStars = resolvedStars;
    }

    public String getResolvedText() {
        return resolvedText;
    }

    public void setResolvedText(String resolvedText) {
        this.resolvedText = resolvedText;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }
    
}
