package com.floorwatch.common.pojos;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SimpleUserStore {

    private SimpleUser user;
    private SimpleStore store;
    private boolean onDuty;

    public SimpleUser getUser() {
        return user;
    }

    public void setUser(SimpleUser user) {
        this.user = user;
    }

    public SimpleStore getStore() {
        return store;
    }

    public void setStore(SimpleStore store) {
        this.store = store;
    }

    public boolean isOnDuty() {
        return onDuty;
    }

    public void setOnDuty(boolean onDuty) {
        this.onDuty = onDuty;
    }
    
}
