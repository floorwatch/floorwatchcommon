package com.floorwatch.common.converters;

import com.floorwatch.common.pojos.SimpleFlare;
import com.floorwatch.entities.Flares;

public class FlareConverter {
    
    private FlareConverter() {
    }
    
    public static Flares toEntity(SimpleFlare simpleFlare) {
        Flares flare = new Flares();
        return flare;
    }
    
    public static SimpleFlare toPojo(Flares flare) {
        SimpleFlare simpleFlare = new SimpleFlare();
        simpleFlare.setId(flare.getId());
        simpleFlare.setCustomerUser(UserConverter.toPojo(flare.getCustomerUserId()));
        simpleFlare.setCustomerText(flare.getCustomerText());
        simpleFlare.setStore(StoreConverter.toPojo(flare.getStoreId()));
        simpleFlare.setCreatedAt(flare.getCreatedAt().getTime());
        simpleFlare.setManagerUser(flare.getManagerUserId() != null ? UserConverter.toPojo(flare.getManagerUserId()) : null);
        simpleFlare.setManagerText(flare.getManagerText() != null ? flare.getManagerText() : null);
        simpleFlare.setCustomerFollowupText(flare.getCustomerFollowupText() != null ? flare.getCustomerFollowupText() : null);
        simpleFlare.setManagerFollowupText(flare.getManagerFollowupText() != null ? flare.getManagerFollowupText() : null);
        simpleFlare.setResolved(flare.getResolved() == 1);
        simpleFlare.setResolvedAt(flare.getResolvedAt() != null ? flare.getResolvedAt().getTime() : null);
        simpleFlare.setResolvedStars(flare.getResolvedStars() != null ? new Integer(flare.getResolvedStars()) : null);
        simpleFlare.setResolvedText(flare.getResolvedText() != null ? flare.getResolvedText() : null);
        return simpleFlare;
    }
    
}
