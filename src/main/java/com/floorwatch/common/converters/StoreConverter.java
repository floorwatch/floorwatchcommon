package com.floorwatch.common.converters;

import com.floorwatch.common.pojos.SimpleStore;
import com.floorwatch.entities.Stores;

public class StoreConverter {
    
    private StoreConverter() {
    }
    
    public static Stores toEntity(SimpleStore simpleStore) {
        Stores store = new Stores();
        return store;
    }
    
    public static SimpleStore toPojo(Stores store) {
        SimpleStore simpleStore = new SimpleStore();
        simpleStore.setId(store.getId());
        simpleStore.setDescription(store.getDescription());
        simpleStore.setAddress1(store.getAddress1());
        simpleStore.setAddress2(store.getAddress2());
        simpleStore.setCity(store.getCity());
        simpleStore.setState(store.getState());
        simpleStore.setCountry(store.getCountry());
        simpleStore.setPostalCode(store.getPostalCode());
        simpleStore.setPhone(store.getPhone());
        simpleStore.setLatitude(store.getLatitude());
        simpleStore.setLongitude(store.getLongitude());
        simpleStore.setDistance(store.getDistance());
        simpleStore.setCompany(CompanyConverter.toPojo(store.getCompanyId()));
        return simpleStore;
    }
    
}
