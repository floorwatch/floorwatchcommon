package com.floorwatch.common.converters;

import com.floorwatch.common.pojos.SimpleCompany;
import com.floorwatch.entities.Companies;

public class CompanyConverter {
    
    private CompanyConverter() {
    }
    
    public static Companies toEntity(SimpleCompany simpleCompany) {
        Companies company = new Companies();
        return company;
    }
    
    public static SimpleCompany toPojo(Companies company) {
        SimpleCompany simpleCompany = new SimpleCompany();
        simpleCompany.setId(company.getId());
        simpleCompany.setDescription(company.getDescription());
        simpleCompany.setPhone(company.getPhone());
        simpleCompany.setWebAddress(company.getWebAddress());
        simpleCompany.setFacebookAddress(company.getFacebookAddress());
        simpleCompany.setTwitterHandle(company.getTwitterHandle());
        simpleCompany.setLogo(company.getLogoId().getLogo());
        return simpleCompany;
    }
    
}
