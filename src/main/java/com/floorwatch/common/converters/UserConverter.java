package com.floorwatch.common.converters;

import com.floorwatch.common.pojos.SimpleNetwork;
import com.floorwatch.common.pojos.SimpleRole;
import com.floorwatch.common.pojos.SimpleStore;
import com.floorwatch.common.pojos.SimpleUser;
import com.floorwatch.entities.Networks;
import com.floorwatch.entities.Roles;
import com.floorwatch.entities.UserNetworks;
import com.floorwatch.entities.UserRoles;
import com.floorwatch.entities.UserStores;
import com.floorwatch.entities.Users;
import com.floorwatch.entities.manager.NetworksFacade;
import com.floorwatch.entities.manager.RolesFacade;
import java.util.ArrayList;
import java.util.Date;
import javax.persistence.NoResultException;

public class UserConverter {
    
    private UserConverter() {
    }
    
    public static Users toEntity(SimpleUser simpleUser) {
        Users user = new Users();
        user.setId(null);
        user.setUsername(simpleUser.getUserName());
        user.setEmailAddress(simpleUser.getEmailAddress());
        user.setFirstName(simpleUser.getFirstName());
        user.setLastName(simpleUser.getLastName());
        // Find the network
        if (simpleUser.getNetworks() != null && !simpleUser.getNetworks().isEmpty()) {
            NetworksFacade nf = new NetworksFacade();
            user.setUserNetworksList(new ArrayList());
            // Network
            for (SimpleNetwork network : simpleUser.getNetworks()) {
                Networks dbNetwork = null;
                try {
                   dbNetwork = nf.findByDescription(network.getDescription());
                } catch(NoResultException nre) {
                    throw nre;
                } catch(Exception e) {
                    throw e;
                }
                UserNetworks userNetwork = new UserNetworks();
                userNetwork.setUserId(user);
                userNetwork.setNetworkId(dbNetwork);
                userNetwork.setNetworkUid(network.getUid());
                userNetwork.setProfilePicture(network.getProfilePicture());
                userNetwork.setCreatedAt(new Date());
                userNetwork.setCreatedBy(1);
                userNetwork.setUpdatedAt(new Date());
                userNetwork.setUpdatedBy(1);
                user.getUserNetworksList().add(userNetwork);
            }
            // Role
            user.setUserRolesList(new ArrayList());
            RolesFacade rf = new RolesFacade();
            Roles dbRole = null;
            try {
                dbRole = rf.findByDescription("Basic");
            } catch (NoResultException nre) {
                throw nre;
            } catch (Exception e) {
                throw e;
            }
            UserRoles userRole = new UserRoles();
            userRole.setUserId(user);
            userRole.setRoleId(dbRole);
            userRole.setCreatedAt(new Date());
            userRole.setCreatedBy(1);
            userRole.setUpdatedAt(new Date());
            userRole.setUpdatedBy(1);
            user.getUserRolesList().add(userRole);
            // No passwords, etc. if network user
            user.setActivatedAt(null);
            user.setActivationCode(null);
            user.setPassword(null);
            user.setResetCode(null);
            user.setSalt(null);
        }        
        return user;
    }
    
    public static SimpleUser toPojo(Users user) {
        SimpleUser simpleUser = new SimpleUser();
        simpleUser.setId(user.getId());
        simpleUser.setUserName(user.getUsername());
        simpleUser.setFirstName(user.getFirstName());
        simpleUser.setLastName(user.getLastName());
        simpleUser.setEmailAddress(user.getEmailAddress());
        simpleUser.setNetworks(new ArrayList());
        if (user.getUserNetworksList() != null && !user.getUserNetworksList().isEmpty()) {
            for (UserNetworks userNetwork : user.getUserNetworksList()) {
                SimpleNetwork simpleNetwork = new SimpleNetwork();
                simpleNetwork.setDescription(userNetwork.getNetworkId().getDescription());
                simpleNetwork.setUid(userNetwork.getNetworkUid());
                simpleNetwork.setProfilePicture(userNetwork.getProfilePicture());
                simpleUser.getNetworks().add(simpleNetwork);
            }
        }    
        simpleUser.setRoles(new ArrayList());
        if (user.getUserRolesList() != null && !user.getUserRolesList().isEmpty()) {
            for (UserRoles userRole : user.getUserRolesList()) {
                SimpleRole simpleRole = new SimpleRole();
                simpleRole.setDescription(userRole.getRoleId().getDescription());
                simpleUser.getRoles().add(simpleRole);
            }
        }
        simpleUser.setStores(new ArrayList());
        if (user.getUserStoresList() != null && !user.getUserStoresList().isEmpty()) {
            for (UserStores userStore : user.getUserStoresList()) {
                SimpleStore simpleStore = new SimpleStore();
                simpleStore.setId(userStore.getStoreId().getId());
                simpleStore.setDescription(userStore.getStoreId().getDescription());
                simpleStore.setAddress1(userStore.getStoreId().getAddress1());
                simpleStore.setAddress2(userStore.getStoreId().getAddress2());
                simpleStore.setCity(userStore.getStoreId().getCity());
                simpleStore.setState(userStore.getStoreId().getState());
                simpleStore.setCountry(userStore.getStoreId().getCountry());
                simpleStore.setPostalCode(userStore.getStoreId().getPostalCode());
                simpleStore.setLatitude(userStore.getStoreId().getLatitude());
                simpleStore.setLongitude(userStore.getStoreId().getLongitude());
                simpleUser.getStores().add(simpleStore);
            }
        }        
        return simpleUser;
    }
    
}
