package com.floorwatch.common.utils;

import com.floorwatch.entities.Users;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

/**
 * <H3>UserUtilities</H3>
 *
 * Class with methods for working with a user.
 *
 * This file contains proprietary information of FloorWatch, Inc.
 * Copying or reproduction without prior written approval is prohibited.
 */

public class UserUtilities {

    private static Logger log = Logger.getLogger(UserUtilities.class);
    
    /**
     * Disable instantiation
     *
     * @author  Dale Davis
     * @date    11/4/2015
     */
    private UserUtilities() {}     
    
    public static boolean authenticate(Users user, String password) {
        String encryptedPassword = null;
        try {
            encryptedPassword = encryptPassword(password, user.getSalt());
            log.debug("password :: " + password + " encrypted password :: " + encryptedPassword );
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
        String hashedPassword = user.getPassword();
        log.debug("db hashedPassword :: " + hashedPassword);
        if (!encryptedPassword.equals(hashedPassword)) {
            log.debug("password does not match");
            return false;
        } else {
            log.debug("password matches");
            return true;
        }    
    }
    
    public static String encryptPassword(String password, String salt) {
        String encryptedPassword = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.reset();
            
            String passwordSaltConcat = "--" + salt + "--" + password + "--";
            
            byte[] bytes = digest.digest(passwordSaltConcat.getBytes());
            char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8','9', 'a', 'b', 'c', 'd', 'e', 'f' };            
            StringBuffer s = new StringBuffer(bytes.length * 2);
            for (int i = 0; i < bytes.length; i++) {
                byte b = bytes[i];
                s.append(digits[(b & 0xf0) >> 4]);
                s.append(digits[b & 0x0f]);
            }
            encryptedPassword = s.toString();
        } catch(Exception e) {
            throw new RuntimeException("Encryption error:" + e.getMessage(), e);
        }
        return encryptedPassword;
    }    
    
    public static String generatePassword() {
        String validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        String password = "";
        for ( int i=1; i < 11; i++ ) {
            password += validChars.charAt(((Double)(Math.random() * validChars.length())).intValue());
        }
        return password;
    }    
  
    public static String generateSalt() {
        String salt = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.reset();
            String currentTime = "--" + Calendar.getInstance().toString() + "--";
            byte[] bytes = digest.digest(currentTime.getBytes());
            char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8','9', 'a', 'b', 'c', 'd', 'e', 'f' };            
            StringBuffer s = new StringBuffer(bytes.length * 2);
            for (int i = 0; i < bytes.length; i++) {
                byte b = bytes[i];
                s.append(digits[(b & 0xf0) >> 4]);
                s.append(digits[b & 0x0f]);
            }
            salt = s.toString();
        } catch(Exception e) {
            throw new RuntimeException("Encryption error:" + e.getMessage(), e);
        }
        return salt;
    }    
    
    /**
     * 
     * @return 
     */
    public static String generateActivationCode(){
        String dateStr = Calendar.getInstance().toString();
        String shuffled = shuffle(dateStr);
        String activationCode = DigestUtils.md5Hex(shuffled);
        return activationCode;
    }
    
    public static String makePasswordResetCode() {
        String passwordResetCode = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.reset();
            String currentTime = "--" + Calendar.getInstance().toString() + "--";
            byte[] bytes = digest.digest(currentTime.getBytes());
            char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8','9', 'a', 'b', 'c', 'd', 'e', 'f' };            
            StringBuffer s = new StringBuffer(bytes.length * 2);
            for (int i = 0; i < bytes.length; i++) {
                byte b = bytes[i];
                s.append(digits[(b & 0xf0) >> 4]);
                s.append(digits[b & 0x0f]);
            }
            passwordResetCode = s.toString();
            passwordResetCode = shuffle(passwordResetCode);
        } catch(Exception e) {
            throw new RuntimeException("Encryption error:" + e.getMessage(), e);
        }
        return passwordResetCode;
    }
    
    public static String shuffle(String input){
        List<Character> characters = new ArrayList<Character>();
        for(char c : input.toCharArray()){
            characters.add(c);
        }
        StringBuilder output = new StringBuilder(input.length());
        while(characters.size()!=0){
            int randPicker = (int)(Math.random()*characters.size());
            output.append(characters.remove(randPicker));
        }
        return output.toString();
    }
}
