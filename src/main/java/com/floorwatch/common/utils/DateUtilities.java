package com.floorwatch.common.utils;

public class DateUtilities {

    public static final long TIME_DIFFERENCE = 946684800L;

    /**
     * Disable instantiation
     *
     * @author Dale Davis
     * @date 12/30/2015
     */
    private DateUtilities() {
    }

    public static long convertMillisSince1970ToSecsSince2000(long millisSince1970) {
        long millisSince2000 = millisSince1970 - TIME_DIFFERENCE * 1000; //number of ms between 1/1/1970 (java epoch) and 1/1/2000 (onset epoch)
        return millisSince2000 / 1000;
    }

    public static long convertSecsSince2000ToMillisSince1970(long secsSince2000) {
        long millisSince2000 = secsSince2000 * 1000;
        return millisSince2000 + TIME_DIFFERENCE * 1000;
    }
}

